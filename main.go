package main

import (
	"flag"
	"io"
	"log"
	"os"
)

func main() {
	inNme := flag.String("in", "input.txt", "The input file")
	outName := flag.String("out", "", "The output file, or empty to print to screen")
	flag.Parse()

	in, err := os.Open(*inNme)
	if err != nil {
		log.Fatal(err)
	}
	defer in.Close()

	var out io.Writer
	if *outName != "" {
		f, err := os.Create(*outName)
		if err != nil {
			log.Fatal(err.Error())
		}
		defer f.Close()
		out = f
	} else {
		out = os.Stdout
	}

	app := NewApp(in, out)

	err = app.Run()
	if err != nil {
		log.Fatalf(err.Error())
	}
}
