package main

import "time"

type Instruction interface {
	Exec(Auction) error
}

type HBInstruction struct {
}

func (i HBInstruction) Exec(Auction) error {
	return nil
}

type BidInstruction struct {
	ItemCode string
	UserID   int64

	Amount float64
}

func (i BidInstruction) Exec(a Auction) error {
	return a.Bid(i.ItemCode, i.UserID, i.Amount)
}

type SellInstruction struct {
	ItemCode string
	UserID   int64

	StartTime    time.Time
	EndTime      time.Time
	ReservePrice float64
}

func (i SellInstruction) Exec(a Auction) error {
	return a.Sell(i.ItemCode, i.UserID, i.ReservePrice, i.StartTime, i.EndTime)
}
