package main

import (
	"io"
	"log"
	"time"
)

type App struct {
	auction Auction
	reader  Reader
	writer  Writer
	time    time.Time
}

type AppOption func(*App)

func WithAuctionAppOption(a Auction) func(*App) {
	return func(app *App) {
	}
}

func NewApp(reader io.Reader, writer io.Writer) *App {
	a := &App{
		reader: NewReader(reader),
		writer: NewWriter(writer),
		time:   time.Time{},
	}
	a.auction = NewAuction(AuctionTimeSourceOption(a.getTime))
	return a
}

func (a *App) getTime() time.Time {
	return a.time
}

func (a *App) updateTime(t time.Time) {
	// TODO make sure time is greater then previous
	a.time = t
}

func (a *App) Run() error {
	for {
		inst, ts, err := a.reader.Next()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}

		a.updateTime(ts)

		err = inst.Exec(a.auction)
		if err != nil {
			log.Printf(err.Error())
		}

		for _, itemCode := range a.auction.List() {
			item, err := a.auction.Get(itemCode)
			if err != nil {
				log.Printf("Error getting item %v - Err: %v", itemCode, err)
			}

			if item.Closed {
				err = a.writer.Write(item)
				if err != nil {
					panic(err)
				}
				a.auction.Delete(itemCode)
			}
		}
	}
}
