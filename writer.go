package main

import (
	"fmt"
	"io"
)

// Writer is an interface to write the result somewhere
type Writer interface {
	Write(Item) error
}

type actualWriter struct {
	writer io.Writer
	count  int // The number of items written
}

// NewWriter creates a new Output
func NewWriter(writer io.Writer) *actualWriter {
	return &actualWriter{
		writer: writer,
	}
}

func (o *actualWriter) Write(item Item) (err error) {
	status := "UNSOLD"
	if item.Sold {
		status = "SOLD"
	}
	userID := ""
	if item.Sold {
		userID = fmt.Sprintf("%v", item.WinningUserID)
	}

	var lowestBid, highestBid float64
	if len(item.Bids) > 0 {
		lowestBid, highestBid, err = calcHighestAndLowestBids(item)
		if err != nil {
			return err // Hmmm
		}
	}

	fmt.Fprintf(o.writer, "%d|%v|%v|%v|%.2f|%v|%.2f|%.2f\n",
		item.EndTime.Unix(),
		item.ItemCode,
		userID,
		status,
		item.PricePaid,
		len(item.Bids),
		highestBid,
		lowestBid,
	)
	return nil
}

func calcHighestAndLowestBids(item Item) (lo float64, hi float64, err error) {
	if len(item.Bids) == 0 {
		return 0.0, 0.0, fmt.Errorf("Need to be at least 1 bid")
	}
	hi = item.Bids[0].Amount
	lo = hi
	for _, bid := range item.Bids[1:] {
		if bid.Amount > hi {
			hi = bid.Amount
		}
		if bid.Amount < lo {
			lo = bid.Amount
		}
	}
	return lo, hi, nil
}
