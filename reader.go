package main

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"
)

// Reader reads instructions from an input
type Reader interface {
	// Next returns the next instruction, the timestamp and any errors
	// if finished the io.EOF is returned
	Next() (Instruction, time.Time, error)
}

// InstructionDecoder decodes a row
type InstructionDecoder func(values []string) (Instruction, error)

// TextReader reads from a pip delimeted text stream
type actualReader struct {
	scanner *bufio.Scanner
}

// NewTextReader creates a TextInput
func NewReader(r io.Reader) *actualReader {
	return &actualReader{
		scanner: bufio.NewScanner(r),
	}
}

// Next returns the next instruction
func (r actualReader) Next() (Instruction, time.Time, error) {
	var raw string
	if !r.scanner.Scan() {
		if r.scanner.Err() != nil {
			return nil, time.Time{}, r.scanner.Err()
		}
		return nil, time.Time{}, io.EOF
	}

	raw = strings.TrimSpace(r.scanner.Text())
	values := strings.Split(raw, "|")

	unixTimestamp, err := strconv.ParseInt(values[0], 10, 64)
	if err != nil {
		return nil, time.Time{}, fmt.Errorf("Failed to part timestamp in %v", raw)
	}
	timestamp := time.Unix(unixTimestamp, 0)

	for _, decode := range rowDecoders {
		instruction, err := decode(values)
		if err == nil {
			return instruction, timestamp, nil
		}
	}

	return nil, timestamp, fmt.Errorf("Invalid instruction - %v", raw)
}

var (
	rowDecoders = []func(values []string) (Instruction, error){
		hbInstructionDecoder,
		sellInstructionDecoder,
		bidInstructionDecoder,
	}
)

func hbInstructionDecoder(values []string) (Instruction, error) {
	if len(values) != 1 {
		return nil, fmt.Errorf("Not HB")
	}

	return HBInstruction{}, nil
}

func sellInstructionDecoder(values []string) (Instruction, error) {
	if len(values) != 6 || values[2] != "SELL" {
		return nil, fmt.Errorf("Invalid")
	}

	userID, err := strconv.ParseInt(values[1], 10, 64)
	if err != nil {
		return nil, err
	}

	item := values[3]

	reservePrice, err := strconv.ParseFloat(values[4], 64)
	if err != nil {
		return nil, fmt.Errorf("Invalid reserve price")
	}

	unixCloseTime, err := strconv.ParseInt(values[5], 10, 64)
	if err != nil {
		return nil, err
	}
	closeTime := time.Unix(unixCloseTime, 0)

	return SellInstruction{
		UserID:       userID,
		ItemCode:     item,
		ReservePrice: reservePrice,
		EndTime:      closeTime,
	}, nil
}

func bidInstructionDecoder(values []string) (Instruction, error) {
	if len(values) != 5 || values[2] != "BID" {
		return nil, fmt.Errorf("Invalid")
	}

	userID, err := strconv.ParseInt(values[1], 10, 64)
	if err != nil {
		return nil, err
	}

	if len(values) != 5 {
		return nil, fmt.Errorf("Invalid record")
	}

	item := values[3]

	amount, err := strconv.ParseFloat(values[4], 64)
	if err != nil {
		return nil, fmt.Errorf("Invalid reserve price")
	}

	return BidInstruction{
		UserID:   userID,
		ItemCode: item,
		Amount:   amount,
	}, nil
}
