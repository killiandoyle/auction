package main

import (
	"bytes"
	"log"
	"testing"
	"time"
)

func TestWriterWriterValidSoldItem(t *testing.T) {
	buff := bytes.NewBuffer(nil)
	w := NewWriter(buff)
	w.Write(Item{
		EndTime:       time.Unix(20, 0),
		ItemCode:      "toaster_1",
		WinningUserID: 8,
		Sold:          true,
		PricePaid:     12.50,
		Bids: []Bid{
			{
				UserID: 2,
				Time:   time.Unix(10, 0),
				Amount: 7.5,
			},
			{
				UserID: 3,
				Time:   time.Unix(10, 0),
				Amount: 12.5,
			},
			{
				UserID: 3,
				Time:   time.Unix(10, 0),
				Amount: 20.,
			},
		},
	})
	exp := "20|toaster_1|8|SOLD|12.50|3|20.00|7.50\n"
	if buff.String() != exp {
		log.Fatalf("%v != %v", buff.String(), exp)
	}
}

func TestWriterWriteUNSOLD(t *testing.T) {
	writer := bytes.NewBuffer(nil)
	output := NewWriter(writer)
	output.Write(Item{
		EndTime:       time.Unix(20, 0),
		ItemCode:      "tv_1",
		WinningUserID: 0,
		Sold:          false,
		PricePaid:     0.0,
		Bids: []Bid{
			{
				UserID: 2,
				Time:   time.Unix(12, 1),
				Amount: 200.0,
			},
			{
				UserID: 2,
				Time:   time.Unix(12, 1),
				Amount: 150.0,
			},
		},
	})
	exp := "20|tv_1||UNSOLD|0.00|2|200.00|150.00\n"
	if writer.String() != exp {
		log.Fatalf("%v != %v", writer.String(), exp)
	}
}

func TestWriterWriteMultipleLines(t *testing.T) {
	writer := bytes.NewBuffer(nil)
	output := NewWriter(writer)
	output.Write(Item{
		EndTime:       time.Unix(20, 0),
		ItemCode:      "toaster_1",
		WinningUserID: 8,
		Sold:          true,
		PricePaid:     12.50,
		Bids: []Bid{
			{
				UserID: 2,
				Time:   time.Unix(2, 0),
				Amount: 10.0,
			},
		},
	})
	output.Write(Item{
		EndTime:       time.Unix(20, 0),
		ItemCode:      "toaster_1",
		WinningUserID: 8,
		Sold:          true,
		PricePaid:     12.50,
		Bids: []Bid{
			{
				UserID: 2,
				Time:   time.Unix(2, 0),
				Amount: 10.0,
			},
		},
	})
	exp := `20|toaster_1|8|SOLD|12.50|1|10.00|10.00
20|toaster_1|8|SOLD|12.50|1|10.00|10.00
`
	if writer.String() != exp {
		log.Fatalf("%v != %v", writer.String(), exp)
	}
}
