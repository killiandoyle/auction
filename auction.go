package main

import (
	"fmt"
	"time"
)

type Auction interface {
	Sell(itemCode string, userID int64, reservePrice float64, startTime, endTime time.Time) error
	Bid(itemCode string, userID int64, amount float64) error
	Get(itemCode string) (Item, error)
	List() (itemCodes []string)
	Delete(itemCode string) error
}

type actualAuction struct {
	items map[string]*auctionItem
	now   func() time.Time
}

type auctionItem struct {
	code         string
	userID       int64
	reservePrice float64
	startTime    time.Time
	endTime      time.Time
	bids         []Bid
}

type Bid struct {
	UserID int64
	Time   time.Time
	Amount float64
}

type Item struct {
	ItemCode string
	EndTime  time.Time
	Bids     []Bid
	Closed   bool

	// fields for closed
	Sold          bool
	WinningUserID int64
	PricePaid     float64
}

func defaultTimeSource() time.Time {
	return time.Now()
}

type AuctionOption func(*actualAuction)

func AuctionTimeSourceOption(f func() time.Time) func(*actualAuction) {
	return func(a *actualAuction) {
		a.now = f
	}
}

func NewAuction(opts ...AuctionOption) *actualAuction {
	a := &actualAuction{
		items: make(map[string]*auctionItem),
		now:   defaultTimeSource,
	}
	for _, f := range opts {
		f(a)
	}
	return a
}

func (a *actualAuction) Sell(itemCode string, userID int64, reservePrice float64, startTime, endTime time.Time) error {
	if _, exists := a.items[itemCode]; exists {
		return fmt.Errorf("Item already exists")
	}
	a.items[itemCode] = &auctionItem{
		code:         itemCode,
		userID:       userID,
		reservePrice: reservePrice,
		startTime:    startTime,
		endTime:      endTime,
	}
	// TODO check id already exists
	return nil
}

func (a *actualAuction) Bid(itemCode string, userID int64, amount float64) error {
	item, exists := a.get(itemCode)
	if !exists {
		return fmt.Errorf("Item '%v' not available for bidding", itemCode)
	}
	bidTime := a.now()
	if !item.startTime.Before(bidTime) {
		return fmt.Errorf("Item auction has not started yet")
	}
	if bidTime.After(item.endTime) {
		return fmt.Errorf("Item auction has ended for item %v", itemCode)
	}
	for _, bid := range item.bids {
		if bid.UserID == userID && amount <= bid.Amount {
			return fmt.Errorf("Bid price must be more than previous bids for user")
		}
	}
	item.bids = append(item.bids, Bid{
		UserID: userID,
		Time:   bidTime,
		Amount: amount,
	})
	return nil
}

func (a *actualAuction) get(itemCode string) (*auctionItem, bool) {
	item, exists := a.items[itemCode]
	return item, exists
}

func (a *actualAuction) calcWinner(item *auctionItem) (sold bool, userID int64, amount float64, err error) {
	if len(item.bids) == 0 {
		return false, 0, 0, nil
	}
	highestBid := item.bids[0]
	price := item.reservePrice
	for _, bid := range item.bids[1:] {
		if bid.Amount > highestBid.Amount {
			price = highestBid.Amount
			highestBid = bid
		}
	}
	return true, highestBid.UserID, price, nil
}

func (a *actualAuction) List() []string {
	itemCodes := make([]string, 0)
	for itemCode := range a.items {
		itemCodes = append(itemCodes, itemCode)
	}
	return itemCodes
}

func (a *actualAuction) isClosed(item *auctionItem) bool {
	return a.now().After(item.endTime)
}

func (a *actualAuction) Get(itemCode string) (Item, error) {
	item, okay := a.get(itemCode)
	if !okay {
		return Item{}, fmt.Errorf("Not found")
	}

	it := Item{
		ItemCode: item.code,
		EndTime:  item.endTime,
	}
	it.Bids = append(it.Bids, item.bids...)
	it.Closed = a.isClosed(item)
	if it.Closed {
		var err error
		it.Sold, it.WinningUserID, it.PricePaid, err = a.calcWinner(item)
		if err != nil {
			return Item{}, err
		}
	}

	return it, nil
}

func (a *actualAuction) Delete(itemCode string) error {
	_, found := a.get(itemCode)
	if !found {
		return fmt.Errorf("Not found")
	}
	delete(a.items, itemCode)
	return nil
}
