package main

import (
	"testing"
	"time"
)

func TestAuctionDuplicateItemsNotAllowed(t *testing.T) {
	a := NewAuction()

	err := a.Sell("itemid-1", 0, 100, time.Now(), time.Now().Add(time.Minute))
	if err != nil {
		t.Fail()
	}

	err = a.Sell("itemid-1", 0, 100, time.Now(), time.Now().Add(time.Minute))
	if err == nil {
		t.Fail()
	}
}

func TestAuctionBidFailsIfNoItem(t *testing.T) {
	a := NewAuction()
	err := a.Bid("itemid-1", 1, 10)
	if err == nil {
		t.Fail()
	}
}

func TestAuctionBidMustFailIfBeforeStart(t *testing.T) {
	a := NewAuction()
	a.Sell("item-1", 1, 10, time.Now().Add(time.Minute), time.Now().Add(time.Hour))
	err := a.Bid("item-1", 2, 20)
	if err == nil {
		t.Fail()
	}
}

func TestAuctionBidMustFailIfOnStartTime(t *testing.T) {
	ts := time.Now()
	a := NewAuction(AuctionTimeSourceOption(func() time.Time { return ts }))
	a.Sell("item-1", 1, 10, ts, ts.Add(time.Minute))
	err := a.Bid("item-1", 1, 20)
	if err == nil {
		t.Fail()
	}
}

func TestAuctionBidMustFailIfHappensAfterEndTime(t *testing.T) {
	ts := time.Now()
	a := NewAuction(AuctionTimeSourceOption(func() time.Time { return ts }))
	a.Sell("item-1", 1, 10, ts, ts.Add(time.Minute))
	ts = ts.Add(time.Hour)
	err := a.Bid("item-1", 2, 20)
	if err == nil {
		t.Fail()
	}
}

func TestAuctionBidMustSucceedIfHappensOnEndTime(t *testing.T) {
	ts := time.Now()
	a := NewAuction(AuctionTimeSourceOption(func() time.Time { return ts }))
	end := ts.Add(time.Minute)
	a.Sell("item-1", 1, 10, ts, end)
	ts = end
	err := a.Bid("item-1", 2, 20)
	if err != nil {
		t.Fail()
	}
}

func TestAuctionBidMustSucceedIfBetweenStartAndEndTime(t *testing.T) {
	ts := time.Now()
	a := NewAuction(AuctionTimeSourceOption(func() time.Time { return ts }))
	a.Sell("item-1", 1, 10, ts, ts.Add(time.Minute))
	ts = ts.Add(time.Second)
	err := a.Bid("item-1", 2, 20)
	if err != nil {
		t.Fail()
	}
}

// * Is larger than any previous valid bids submitted by the user.

func TestAuctionNotValidIfLessThanPreviousBid(t *testing.T) {
	a := NewAuction()
	a.Sell("item-1", 1, 10, time.Now(), time.Now().Add(time.Hour))
	var userID int64 = 2
	a.Bid("item-1", userID, 10)
	err := a.Bid("item-1", userID, 8)
	if err == nil {
		t.Fail()
	}
}

func TestAuctionBidNotValidIfUserHasSameAsPreviousBid(t *testing.T) {
	a := NewAuction()
	a.Sell("item-1", 1, 10, time.Now(), time.Now().Add(time.Hour))
	var userID int64 = 2
	a.Bid("item-1", userID, 10)
	err := a.Bid("item-1", userID, 10)
	if err == nil {
		t.Fail()
	}
}

func TestAuctionWinnerIsFirstBidWhenMultipleHighestBidsOfSameAmount(t *testing.T) {
	ts := time.Now()
	a := NewAuction(AuctionTimeSourceOption(func() time.Time { return ts }))
	a.Sell("item-1", 1, 10, ts, ts.Add(time.Minute))
	ts = ts.Add(time.Second)
	a.Bid("item-1", 2, 20.)
	ts = ts.Add(time.Second)
	a.Bid("item-1", 3, 20.)
	ts = ts.Add(time.Minute)
	item, err := a.Get("item-1")
	if err != nil {
		t.Fail()
	}
	if item.WinningUserID != 2 {
		t.Fail()
	}
}
