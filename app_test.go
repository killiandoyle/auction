package main

import (
	"bytes"
	"testing"
)

func TestApp(t *testing.T) {
	in := bytes.NewBuffer(nil)
	out := bytes.NewBuffer(nil)
	a := NewApp(in, out)

	in.WriteString("10|1|SELL|toaster_1|10.00|20\n")
	in.WriteString("12|8|BID|toaster_1|7.50\n")
	in.WriteString("13|5|BID|toaster_1|12.50\n")
	in.WriteString("16\n")
	in.WriteString("17|8|BID|toaster_1|20.00\n")
	in.WriteString("21\n")
	in.Write(nil)

	err := a.Run()
	if err != nil {
		t.Errorf(err.Error())
	}
	if out.String() != "20|toaster_1|8|SOLD|12.50|3|20.00|7.50\n" {
		t.Fail()
	}
}
