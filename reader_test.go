package main

import (
	"io"
	"strings"
	"testing"
	"time"
)

func TestReaderWithValidSell(t *testing.T) {
	r := strings.NewReader(`10|1|SELL|toaster_1|10.00|20`)
	inp := NewReader(r)
	inst, ts, err := inp.Next()
	if err != nil {
		t.Errorf("error should be nil but got - %v", err)
	}
	exp := SellInstruction{
		UserID:       1,
		ItemCode:     "toaster_1",
		ReservePrice: 10,
		EndTime:      time.Unix(20, 0),
	}
	if ts.Unix() != 10 {
		t.Errorf("Invalid timestamp")
	}
	if inst != exp {
		t.Errorf("Invalid User ID")
	}
}

func TestReaderWithValidBid(t *testing.T) {
	r := strings.NewReader(`12|8|BID|toaster_1|7.50`)
	inp := NewReader(r)
	inst, ts, err := inp.Next()
	if err != nil {
		t.Errorf("Instruction should be nil got - %v", err)
	}
	exp := BidInstruction{
		UserID:   8,
		ItemCode: "toaster_1",
		Amount:   7.5,
	}
	if ts.Unix() != 12 {
		t.Errorf("Invalid timestamp")
	}
	if inst != exp {
		t.Errorf("Invalid %v != %v", inst, exp)
	}
}

func TestReaderWithValidHB(t *testing.T) {
	r := strings.NewReader(`12`)
	inp := NewReader(r)
	inst, ts, err := inp.Next()
	if err != nil {
		t.Errorf("Instruction should be nil got - %v", err)
	}
	exp := HBInstruction{}
	if ts.Unix() != 12 {
		t.Errorf("Invalid timestamp")
	}
	if inst != exp {
		t.Errorf("Invalid %v != %v", inst, exp)
	}
}

func TestReaderWithMultipleInstructions(t *testing.T) {
	r := strings.NewReader(`10|1|SELL|toaster_1|10.00|20
16`)
	inp := NewReader(r)

	inst, ts, err := inp.Next()
	if err != nil {
		t.Errorf("Instruction should be nil got - %v", err)
	}
	if ts.Unix() != 10 {
		t.Errorf("Invalid timestamp")
	}
	exp2 := SellInstruction{
		UserID:       1,
		ItemCode:     "toaster_1",
		ReservePrice: 10,
		EndTime:      time.Unix(20, 0),
	}
	if inst != exp2 {
		t.Errorf("Invalid %v != %v", inst, exp2)
	}

	inst, ts, err = inp.Next()
	if err != nil {
		t.Errorf("Instruction should be nil got - %v", err)
	}
	exp1 := HBInstruction{}
	if ts.Unix() != 16 {
		t.Errorf("Invalid timestamp")
	}
	if inst != exp1 {
		t.Errorf("Invalid %v != %v", inst, exp1)
	}
}

func TestReaderReturnsEOFWhenFinished(t *testing.T) {
	r := strings.NewReader(``)
	inp := NewReader(r)

	_, _, err := inp.Next()
	if err != io.EOF {
		t.Errorf("EOF not returned")
	}
}

func TestReaderFailsWithInvalidRow(t *testing.T) {
	r := strings.NewReader(`asdf`)
	inp := NewReader(r)

	_, _, err := inp.Next()
	if err == io.EOF || err == nil {
		t.Errorf("Error should have been returned")
	}
}
